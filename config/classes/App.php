<?php 
namespace Config\Classes;

abstract class App {
    
    private static $_connexion = null, $_rootPublic, $_rootAdmin, $_start = false;

    // GETTERS
    public function getConnexion() {
        return SELF::$_connexion;
    }

    public function getRootPublic() {
        return SELF::$_rootPublic;
    }

    public function getRootAdmin() {
        return SELF::$_rootAdmin;
    }



    // SETTERS
    private static function setConnexion() {
        
        $dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME;
        $param = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

        try {
            SELF::$_connexion = new \PDO($dsn, DB_USER, DB_PWD, $param);
        } catch (\PDOException $e) {
            die("Problème de connexion à la base de donnée : " . $e);
        }

    }

    private static function setRootPublic() {
        $local_path = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
        SELF::$_rootPublic = 'http://' . $_SERVER['HTTP_HOST'] . $local_path;
    }

    private static function setRootAdmin() {
        SELF::$_rootAdmin = str_replace(PUBLIC_FOLDER, BACKOFFICE_FOLDER, SELF::$_rootPublic);
    }



    // AUTRES METHODES
    public static function start() {
        if (!SELF::$_start) {
            SELF::setRootPublic();
            SELF::setRootAdmin();
            SELF::setConnexion();
            SELF::$_start = true;
        }
    }

    public static function close() {
        SELF::$_connexion = null;
    }

}


