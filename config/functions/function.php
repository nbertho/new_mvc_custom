<?php

/**
 * Function to set a link relativly to public path
 *
 * @param   string  $str    path of the link
 * @param   array   $param  Optionnal parametres for the link
 *
 * @return  string          return the link with all the parametres
 */
function asset(string $str, array $param = null):string {
    // If parametres are given
    if ($param) {
        $link = '';
        foreach ($param as $item) {
            $link = $link . '/' . $item;
        }
        return SERVER_ROOT_PUBLIC . '/' . $str . '/' . $link;
    }
    // If no parametres are given
    else {
        return SERVER_ROOT_PUBLIC . '/' . $str;
    }
}