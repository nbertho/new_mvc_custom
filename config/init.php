<?php

    // Chargement de l'autoloader
    require_once '../vendor/autoload.php';

    // Chargement des paramètres de la base de donnée
    require_once 'settings/env.php';

    // Initialisation des zones dynamiques
    require_once 'settings/dynamicZone.php';

    // Chargement des constantes
    require_once 'settings/const.php';

    // Chargement des fonctions
    require_once 'functions/function.php';