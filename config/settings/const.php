<?php 

// Other const settings

    define('SERVER_ROOT', str_replace('\config\settings', '', __DIR__));
    define('SERVER_ROOT_PUBLIC', SERVER_ROOT . '/public');
    define('SERVER_ROOT_BACKOFFICE', SERVER_ROOT . '/backoffice');
    define('PUBLIC_FOLDER', 'public');
    define('BACKOFFICE_FOLDER', 'backoffice');
