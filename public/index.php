<?php
use \Config\Classes\App as App;


require_once '../config/init.php';


    App::start();

    include_once '../src/router.php';

    // Prevent loading the template if AJAX Request
    if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
        require_once '../app/view/templates/default.php';
    }

    App::close();